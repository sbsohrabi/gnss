#!/bin/bash

#    █████   ███▄ ▄███▓ ██▓
#  ▒██▓  ██▒▓██▒▀█▀ ██▒▓██▒
#  ▒██▒  ██░▓██    ▓██░▒██▒
#  ░██  █▀ ░▒██    ▒██ ░██░
#  ░▒███▒█▄ ▒██▒   ░██▒░██░
#  ░░ ▒▒░ ▒ ░ ▒░   ░  ░░▓
#   ░ ▒░  ░ ░  ░      ░ ▒ ░
#     ░   ░ ░      ░    ▒ ░
#      ░           ░    ░
#
#
# linux qmi_wwan.c backport patch
#
echo "qmi_wwan.c backport, 4.14 -> 4.9.80"
echo ""

for arg in "$@"
do
    case $arg in
       --debug)
	     ENABLE_DEBUGGING=1
       ;;
       --clean)
	     echo "<< cleaning up temporary files ... >>"
         rm -fr /root/linux* > /dev/null 2>&1
         rm -fr /root/$DRIVER_UNPACK_PATH* > /dev/null 2>&1
         echo "Done."
         exit 0
       ;;
       -h|--help )
         echo "usage: $0 [<option>,...]"
         echo ""
         echo " Options: "
         echo " --debug       enable script debugging"
         echo ""
         exit 0
       ;;

    esac
done



if [ "$(uname -r|sed  's/[\.-]/ /g' | awk '{ print $1$2$3; }')" != "4980" ] ; then
   echo "This patch only applies to kernel version 4.9.80! Aborting."
   exit 1
fi

[ "$ENABLE_DEBUGGING" == "1" ] && set -x

build_kernel_module(){
    echo -n "Preparing environment..."
    # cleanup previous mess if any exists.
    rm -fr /root/linux*
    rm -fr /root/*raspberrypi-kernel*

    cat << EOF > /root/qmi_wwan_c.patch
--- linux-a2f34d45809d8685bca1e91989e35746499ac400/drivers/net/usb/qmi_wwan.c	2018-02-09 12:44:19.000000000 +0000
+++ linux-source-modified/drivers/net/usb/qmi_wwan.c	2018-09-02 18:30:11.292567869 +0000
@@ -940,7 +940,7 @@
 	{QMI_FIXED_INTF(0x413c, 0x81b6, 10)},	/* Dell Wireless 5811e */
 	{QMI_FIXED_INTF(0x03f0, 0x4e1d, 8)},	/* HP lt4111 LTE/EV-DO/HSPA+ Gobi 4G Module */
 	{QMI_FIXED_INTF(0x22de, 0x9061, 3)},	/* WeTelecom WPD-600N */
-	{QMI_FIXED_INTF(0x1e0e, 0x9001, 5)},	/* SIMCom 7230E */
+	{QMI_QUIRK_SET_DTR(0x1e0e, 0x9001, 5)},	/* SIMCom 7100E, 7230E, 7600E ++ */
 	{QMI_QUIRK_SET_DTR(0x2c7c, 0x0125, 4)},	/* Quectel EC25, EC20 R2.0  Mini PCIe */
 	{QMI_QUIRK_SET_DTR(0x2c7c, 0x0121, 4)},	/* Quectel EC21 Mini PCIe */
 	{QMI_FIXED_INTF(0x2c7c, 0x0296, 4)},	/* Quectel BG96 */
EOF

    echo "Checking prerequisites..."

    PROC_VERSION_GCC=$(cat /proc/version | sed 's/\(.*gcc version \)//g'|awk '{print $1}')
    GCC_MAJOR_VER=${PROC_VERSION_GCC:0:1}
    GCC_MINOR_VER=${PROC_VERSION_GCC:2:1}
    echo "Linux kernel was compiled using gcc $PROC_VERSION_GCC"

    if [ "$GCC_MAJOR_VER" == "4" ] ; then
        GCC_VERSION="gcc-$GCC_MAJOR_VER.$GCC_MINOR_VER"
        GPP_VERSION="g++-$GCC_MAJOR_VER.$GCC_MINOR_VER"
    else
        GCC_VERSION="gcc-$GCC_MAJOR_VER"
        GPP_VERSION="g++-$GCC_MAJOR_VER"
    fi

    echo -n "Checking for $GCC_VERSION ... "
    RES=$(dpkg -l $GCC_VERSION > /dev/null 2>&1)
    GCC_INSTALLED=$?
    [ "$GCC_INSTALLED" == "0" ] && echo " found!"
    [ "$GCC_INSTALLED" != "0" ] && echo " not found! Will be installed now."

    echo "Running 'apt update' ..."
    apt update

    if [ "$GCC_INSTALLED" != "0" ] ; then
        echo "Installing $GCC_VERSION $GPP_VERSION ... "
        apt install -y $GCC_VERSION $GPP_VERSION
        update-alternatives --remove-all gcc
        update-alternatives --remove-all g++
        update-alternatives --install /usr/bin/gcc gcc /usr/bin/$GCC_VERSION 60 --slave /usr/bin/g++ g++ /usr/bin/$GPP_VERSION
    fi

    echo "Installing git bc libncurses5-dev ... "
    # install prerequisites
    apt install -y git bc libncurses5-dev python3-pip

    echo "Fetching kernel-source for $(uname -r) ... "

    if [ -e /boot/.firmware_revision ]; then
        FIRMWARE_HASH=$(cat /boot/.firmware_revision)
        SOURCE_HASH=$(curl -L -s https://github.com/Hexxeh/rpi-firmware/raw/$FIRMWARE_HASH/git_hash)
        cd /root
        wget -q -O raspberrypi-kernel_$SOURCE_HASH.zip https://github.com/raspberrypi/linux/archive/$SOURCE_HASH.zip
        unzip raspberrypi-kernel_$SOURCE_HASH.zip
        RES=$(rm linux > /dev/null  2>&1)
        ln -s "linux-$SOURCE_HASH" linux
        REPO="Hexxeh/rpi-firmware"
        GIT_PATH=$FIRMWARE_HASH
    else

        KERNEL_SOURCE_VER=`zcat /usr/share/doc/raspberrypi-bootloader/changelog.Debian.gz |head|grep '^raspberrypi-firmware'|head -n1|awk '{print $2; }'|sed -e's/(//' -e's/)//'`
        FIRMWARE_HASH=`zcat /usr/share/doc/raspberrypi-bootloader/changelog.Debian.gz |grep "\* firmware" |awk '{ print $5; }'|head -n1`

        GIT_TAG=$(git ls-remote https://github.com/raspberrypi/firmware.git |grep $FIRMWARE_HASH|head -n1|awk '{ print $2}'| rev | tr "/" "\n" | rev|head -n1)

        echo "GIT_TAG: $GIT_TAG"
        echo "FIRMWARE_HASH: $FIRMWARE_HASH"

        if [ "$GIT_TAG" == "" ]; then
            echo "WARNING: not matching GIT_TAG found for your kernel version."
            echo "         the driver installation will probably fail without an "
            echo "         updated raspberrypi-kernel."
        fi

        HAS_MODULE_SYMVERS=$(curl -I https://github.com/raspberrypi/firmware/raw/$GIT_TAG/extra/Module.symvers  -L -s |grep "HTTP/1.1 200 OK")
        if [ "$HAS_MODULE_SYMVERS" == "" ]; then
            GIT_PATH=${KERNEL_SOURCE_VER:0:$((${#KERNEL_SOURCE_VER}-2))}
        else
            GIT_PATH=$GIT_TAG
        fi
        echo "GIT_PATH: $GIT_PATH"


        cd /root/
        wget -q https://github.com/raspberrypi/linux/archive/raspberrypi-kernel_${KERNEL_SOURCE_VER}.zip
        if [ "$?" != "0" ]; then
            echo "ERROR: failed to download raspberrypi-kernel_${KERNEL_SOURCE_VER}.zip from https://github.com/raspberrypi/linux/archive/"
            echo ""
            echo "please run 'apt install -y raspberrypi-bootloader && reboot' and retry."
            exit 1
        fi
        unzip -q "raspberrypi-kernel_${KERNEL_SOURCE_VER}.zip"
        rm linux > /dev/null  2>&1
        ln -s "linux-raspberrypi-kernel_${KERNEL_SOURCE_VER}" linux

        RES=$(rm /lib/modules/$(uname -r)/{build,source} > /dev/null 2>&1; ln -s /root/linux /lib/modules/$(uname -r)/build ; ln -s /root/linux /lib/modules/$(uname -r)/source)

        REPO="raspberrypi/firmware"
        GIT_PATH="$GIT_PATH/extra"
    fi

    cd /root/linux

    echo "Downloading Module.symvers ... "

    V7=$(uname -a|grep -- '-v7+')
    if [ "$?" == "0" ]; then
        curl -L -s -o Module.symvers https://github.com/$REPO/raw/$GIT_PATH/Module7.symvers
    else
        curl -L -s -o Module.symvers https://github.com/$REPO/raw/$GIT_PATH/Module.symvers
    fi
    if [ "$?" != "0" ]; then
        echo "ERROR: failed to retrieve Modules.symvers. Unable to continue. "
        exit 1
    fi

    # make /proc/config.gz available
    modprobe configs
    # prepare default config
    make bcm2709_defconfig
    # copy running kernel config to build tree
    zcat /proc/config.gz > .config

    if [ "$V7" == "" ] ; then
        # set EXTRAVERSION
        sed -i 's/EXTRAVERSION =.*/EXTRAVERSION = +/' Makefile
    else
        sed -i 's/CONFIG_LOCALVERSION=.*/CONFIG_LOCALVERSION="-v7+"/' .config
    fi

    echo "Checking kernel-source releaseversion..."

    RELEASE=$(make kernelrelease|tail -n 1)
    UNAME_RELEASE=$(uname -r)

    if [ "$RELEASE" != "$UNAME_RELEASE" ]; then
        echo "ERROR: kernel-source tree version $RELEASE does not match running kernel version $UNAME_RELEASE"
        exit 1
    fi

##########
    cd /root/linux

    echo "Applying patch for qmi_wwan.c ..."
    patch -p 1  < /root/qmi_wwan_c.patch

    echo "Compiling modules drivers/net/usb ..."
    DRIVER_PATH=drivers/net/usb

    make olddefconfig && make modules_prepare && make M=$DRIVER_PATH modules
######


    if [ "$?" != "0" ] ; then
        echo "ERROR: failed build kernel module ... "
        exit 1
    else
        echo ""
        echo "Driver built successfully."
		sleep 1
		echo "Installing..."
		cp /lib/modules/4.9.80-v7+/kernel/drivers/net/usb/qmi_wwan.ko /lib/modules/4.9.80-v7+/kernel/drivers/net/usb/qmi_wwan.ko.old
		cp /root/linux/drivers/net/usb/qmi_wwan.ko /lib/modules/4.9.80-v7+/kernel/drivers/net/usb/qmi_wwan.ko
		depmod
		echo "All done, rebooting in 10 Seconds..."
		sleep 10
		reboot
		
    fi
}




build_kernel_module

