#!/usr/bin/python

import time
import bluetooth

import math
import os
from myserial import myserial
from mytilt import mytilt
        



GPN = "$GPNAV"
GPY = "$GPYBM"
STA = "*"

xdi = []
xsi = []



def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xdi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches
def find_all_star(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xsi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches

        

        

tilt = mytilt()
ser = myserial("/dev/ttyUSB0")


accl_xout = tilt.read_word_2c(0x3b)
accl_yout = tilt.read_word_2c(0x3d)
accl_zout = tilt.read_word_2c(0x3f)
accl_xout_scaled = accl_xout / 16384.0
accl_yout_scaled = accl_yout / 16384.0
accl_zout_scaled = accl_zout / 16384.0


server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
#server_sock.settimeout(1)
port = 1
server_sock.bind(('',port))
server_sock.listen(1)
sdcount = 0

time.sleep(2)
ser.send(["LOG GPGSV ONTIME 1", "LOG SATMSGB ONTIME 1"])


while True:
    client_sock,address = server_sock.accept()
    client_sock.settimeout(2)
    client_sock.setblocking(False);
    print 'Accepted connection from ',address

    if ser.isOpen():
        try:
            #write data
            '''
            ser.write("<command>\r\n$$SI\x18\x02\r\n</command>\r\n")
            #ser.write("<command>\r\n$$SI\x18\x02\r\n</command>\r\n".encode())
            time.sleep(0.5)
            cmdSend = "\r\n<command>\r\n"
            cmdSend += "LOG GPGSV ONTIME 1\r\n"
            cmdSend += "LOG SATMSGB ONTIME 1\r\nLOG BESTPOSB ONTIME 1\r\nLOG BESTVELB ONTIME 1\r\nLOG TIMEB ONTIME 1\r\nLOG GPNAV ONTIME 0.1\r\nLOG SATMSG2B ONTIME 1\r\nLOG GPYBM ONTIME 0.1\r\n</command>\r\n"
            #cmdSend += ""
            ser.write(cmdSend);
            '''
            ser.send(["LOG GPGSV ONTIME 1", "LOG SATMSGB ONTIME 1"])
            time.sleep(1.5)
            while True:
                accl_xout = tilt.read_word_2c(0x3b)
                accl_yout = tilt.read_word_2c(0x3d)
                accl_zout = tilt.read_word_2c(0x3f)
                accl_xout_scaled = accl_xout / 16384.0
                accl_yout_scaled = accl_yout / 16384.0
                accl_zout_scaled = accl_zout / 16384.0
                
                # shut down
                
                #print str(accl_zout_scaled)
                if (accl_yout_scaled < -0.9) :
                    sdcount += 1
                else :
                    if (sdcount > 1):
                        sdcount -= 1
                if  sdcount > 20 :
                    client_sock.send("shutting down\r\n")
                    print "shutting down"
                    os.system ("sudo shutdown now -h")
                
                #print "X Rotation: " , tilt.get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
                #print "Y Rotation: " , tilt.get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
                
                #ser.flushInput() #flush input buffer, discarding all its contents
                #ser.flushOutput() #flush output buffer, aborting current output 
                      #and discard all that is in buffer


                ## while True:
                #response = ser.readline()
                #print(response)
                #find_all(response, GPN)
                ##find_all_star(response, '*')
                #print(xdi)
                #i = 1
                #while i < len(xdi):
                #    si = response.find('*', xdi[i])
                #    print(str(si))
                #    if si != -1:
                #        print(response[xdi[i]:si])
                #        client_sock.send(response[di:si] + "\r\n")
                #    print(str(xdi[i]) + " : " + str(si))
                #    i += 1
                #del xdi[:]
                #del xsi[:]

                #print "preSeiral"
                #continue
                response = ser.read()
                #print "info: serial rx [%s]" % response
                di = response.find(GPN)
                si = response.find(STA, di)
                #print "postSeiral"
                
                
                if di != -1 and si != -1 and si > di :
                    #print(response[di:si])
                    client_sock.send(response[di:si] + "#" + str(tilt.get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)) + "," + str(tilt.get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)) + "@" + "\r\n")
                

                #if di != -1
                #print "preRecv"
                
                try:
                    data = client_sock.recv(1024)
                    #print "info: bt received [%s]" % data
                    parts = data.split("*")
                    '''
                    IPSET*STATIC,192.168...,RESET,
                    ADDCMD*GPNAV,GPYBM
                    SETCMD*GPNAV,GPGGA,GP
                    REMCMD*GPNAV,
                    PWROFF*
                    GETBAT*
                    '''                    
                    subparts = parts[1].split(",")
                    cmd = parts[0]
                    if cmd == "IPSET":
                        i = 1
                    elif cmd == "ADDCMD":
                        i = 1
                    elif cmd == "SETCMD":
                        i = 1
                    elif cmd == "REMCMD":
                        i = 1
                    elif cmd == "PWROFF":
                        os.system ("sudo shutdown now -h")
                    elif cmd == "GETBAT":
                        i = 1
                    else:
                        i = 1
                    
                except Exception, e1:
                    i = 1 #print "..." #"error: : " + str(e1)
                
                #print "post_rec"
                #print("\r\n" + response)

                #ser.close()
        except Exception, e1:
            print "error communicating...: " + str(e1)
 
    else:
        print "cannot open serial port "
 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
 
client_sock.close()
server_sock.close()
