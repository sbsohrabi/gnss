#!/usr/bin/python

import serial, time
import bluetooth
import smbus
import math
import os, sys


#initialization and open the port

#possible timeout values:
#    1. None: wait forever, block call
#    2. 0: non-blocking mode, return immediately
#    3. x, x is bigger than 0, float allowed, timeout block call

ser = serial.Serial()
ser.port = str(sys.argv[1])
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
#ser.timeout = None          #block read
ser.timeout = 1            #non-block read
ser.xonxoff = False     #disable software flow control
ser.rtscts = False     #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2     #timeout for write


try:
    ser.open()
except Exception, e:
    print "error open serial port, matbe device is not connected"
    exit()


time.sleep(2)
while True:

    if ser.isOpen():
        ser.write("\r\n<command>\r\n$$SI\x18\x02\r\n</command>\r\n".encode())
        
        time.sleep(0.1)
        '''
        response = ser.readline()
        print(response)
        response = ser.readline()
        print(response)
        response = ser.readline()
        print(response)        
        time.sleep(0.5)
        '''
    else:
        print "cannot open serial port "
 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
 
