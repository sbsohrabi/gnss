#!/usr/bin/python

import serial, time
import bluetooth
import smbus
import math
import os

# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
 

bus = smbus.SMBus(1)                            # bus = smbus.SMBus(0) fuer Revision 1
i2c_address = 0x68                                  # via i2cdetect
bus.write_byte_data(i2c_address, power_mgmt_1, 0)   # Activate to be able to address the module

def read_byte(reg):
    return bus.read_byte_data(i2c_address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(i2c_address, reg)
    l = bus.read_byte_data(i2c_address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def dist(a,b):
    return math.sqrt((a*a)+(b*b))
 
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)
 
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)


accl_xout = read_word_2c(0x3b)
accl_yout = read_word_2c(0x3d)
accl_zout = read_word_2c(0x3f)
accl_xout_scaled = accl_xout / 16384.0
accl_yout_scaled = accl_yout / 16384.0
accl_zout_scaled = accl_zout / 16384.0

#initialization and open the port

#possible timeout values:
#    1. None: wait forever, block call
#    2. 0: non-blocking mode, return immediately
#    3. x, x is bigger than 0, float allowed, timeout block call

ser = serial.Serial()
ser.port = "/dev/ttyUSB0"
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
#ser.timeout = None          #block read
ser.timeout = 1            #non-block read
ser.xonxoff = False     #disable software flow control
ser.rtscts = False     #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2     #timeout for write

GPN = "$GPNAV"
GPY = "$GPYBM"
STA = "*"

xdi = []
xsi = []

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xdi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches
def find_all_star(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xsi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches


try:
    ser.open()
except Exception, e:
    print "error open serial port, matbe device is not connected"
    exit()

server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
#server_sock.setblocking(False);
port = 1
server_sock.bind(('',port))
server_sock.listen(1)
sdcount = 0

time.sleep(2)
while True:
    client_sock,address = server_sock.accept()
    print 'Accepted connection from ',address

    if ser.isOpen():
        try:
            #write data

            ser.write("<command>\r\n$$SI\x18\x02\r\n</command>\r\n".encode())
            time.sleep(0.5)
            cmdSend = "\r\n<command>\r\n"
            cmdSend += "LOG GPGSV ONTIME 1\r\n"
            cmdSend += "LOG SATMSGB ONTIME 1\r\nLOG BESTPOSB ONTIME 1\r\nLOG BESTVELB ONTIME 1\r\nLOG TIMEB ONTIME 1\r\nLOG GPNAV ONTIME 0.1\r\nLOG SATMSG2B ONTIME 1\r\nLOG GPYBM ONTIME 0.1\r\n</command>\r\n"
            #cmdSend += ""
            ser.write(cmdSend.encode());
            time.sleep(1.5)
            while True:
                accl_xout = read_word_2c(0x3b)
                accl_yout = read_word_2c(0x3d)
                accl_zout = read_word_2c(0x3f)
                accl_xout_scaled = accl_xout / 16384.0
                accl_yout_scaled = accl_yout / 16384.0
                accl_zout_scaled = accl_zout / 16384.0
                
                # shut down
                
                #print str(accl_zout_scaled)
                if (accl_yout_scaled < -0.9) :
                    sdcount += 1
                else :
                    if (sdcount > 1):
                        sdcount -= 1
                if  sdcount > 20 :
                    client_sock.send("shutting down\r\n")
                    print "shutting down"
                    os.system ("sudo shutdown now -h")
                
                #print "X Rotation: " , get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
                #print "Y Rotation: " , get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
                
                #ser.flushInput() #flush input buffer, discarding all its contents
                #ser.flushOutput() #flush output buffer, aborting current output 
                      #and discard all that is in buffer


                ## while True:
                #response = ser.readline()
                #print(response)
                #find_all(response, GPN)
                ##find_all_star(response, '*')
                #print(xdi)
                #i = 1
                #while i < len(xdi):
                #    si = response.find('*', xdi[i])
                #    print(str(si))
                #    if si != -1:
                #        print(response[xdi[i]:si])
                #        client_sock.send(response[di:si] + "\r\n")
                #    print(str(xdi[i]) + " : " + str(si))
                #    i += 1
                #del xdi[:]
                #del xsi[:]

                response = ser.readline()
                #print(response)
                di = response.find(GPN)
                si = response.find(STA, di)
                
                
                if di != -1 and si != -1 and si > di :
                    print(response[di:si])
                    client_sock.send(response[di:si] + "#" + str(get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)) + "," + str(get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)) + "@" + "\r\n")
                

                #if di != -1
                #data = client_sock.recv(1024)
                #print 'received [%s]' % data
                #print("\r\n" + response)

                #ser.close()
        except Exception, e1:
            print "error communicating...: " + str(e1)
 
    else:
        print "cannot open serial port "
 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
 
client_sock.close()
server_sock.close()
