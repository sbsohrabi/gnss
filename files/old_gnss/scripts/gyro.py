#!/usr/bin/python
import smbus
import math
import time
# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
 
def read_byte(reg):
    return bus.read_byte_data(address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def dist(a,b):
    return math.sqrt((a*a)+(b*b))
 
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)
 
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)
 
bus = smbus.SMBus(1) # bus = smbus.SMBus(0) fuer Revision 1
address = 0x68       # via i2cdetect
 
# Activate to be able to address the module
bus.write_byte_data(address, power_mgmt_1, 0)

while 1:
    time.sleep( 0.04 )
    print chr(27) + "[2J"
    print "Gyroscope"
    print "--------"
    '''
    gyro_xout = read_word_2c(0x43)
    gyro_yout = read_word_2c(0x45)
    gyro_zout = read_word_2c(0x47)
     
    print "gyro_xout: ", ("%5d" % gyro_xout), " scaled: ", (gyro_xout / 131)
    print "gyro_yout: ", ("%5d" % gyro_yout), " scaled: ", (gyro_yout / 131)
    print "gyro_zout: ", ("%5d" % gyro_zout), " scaled: ", (gyro_zout / 131)
     
    print
    print "acclssensor"
    print "---------------------"
    ''' 
    accl_xout = read_word_2c(0x3b)
    accl_yout = read_word_2c(0x3d)
    accl_zout = read_word_2c(0x3f)

    accl_xout_scaled = accl_xout / 16384.0
    accl_yout_scaled = accl_yout / 16384.0
    accl_zout_scaled = accl_zout / 16384.0
    '''
     
    print "accl_xout: ", ("%6d" % accl_xout), " scaled: ", accl_xout_scaled
    print "accl_yout: ", ("%6d" % accl_yout), " scaled: ", accl_yout_scaled
    print "accl_zout: ", ("%6d" % accl_zout), " scaled: ", accl_zout_scaled
    '''
    print "X Rotation: " , get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
    print "Y Rotation: " , get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled)
