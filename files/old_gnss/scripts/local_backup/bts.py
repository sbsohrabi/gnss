#!/usr/bin/python
'''
import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

'''

import time
import bluetooth

import math
import os
from myserial import myserial
from mytilt import mytilt
from mybt import mybt
         
import array
from struct import unpack
import binascii

import subprocess


'''
RST = 24
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
disp.begin()

# Clear display.
disp.clear()
disp.display()


# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = 2
shape_width = 5
top = padding
bottom = top + shape_width #height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = padding
# Draw an ellipse.
#draw.ellipse((x, top , x+shape_width, bottom), outline=255, fill=0)
x += shape_width+padding
# Draw a rectangle.
#draw.rectangle((x, top, x+shape_width, bottom), outline=255, fill=0)
x += shape_width+padding
# Draw a triangle.
#draw.polygon([(x, bottom), (x+shape_width/2, top), (x+shape_width, bottom)], outline=255, fill=0)
#draw.polygon([(3, 1), (15, 1), (15, 7), (2, 7), (3, 6), (3, 6), (3, 3),(3, 3)], outline=255, fill=0)
#draw.polygon([(8, 3), (11, 3), (11, 5), (8, 5)], outline=255, fill=1)
x += shape_width+padding
# Draw an X.
#draw.line((x, bottom, x+shape_width, top), fill=255)
#draw.line((x, top, x+shape_width, bottom), fill=255)
x += shape_width+padding

# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
#font = ImageFont.truetype('Minecraftia.ttf', 8)

# Write two lines of text.
draw.text((x+10, top-3),    'GNSS',  font=font, fill=255)
draw.text((x-25, top+4), 'BAT: 75%', font=font, fill=255)
draw.text((x+45, top+4),    '4G: ON',  font=font, fill=255)
draw.text((x-10, top+12), 'Mod: NO FIX', font=font, fill=255)
draw.text((x-10, top+20), 'Sat: 18', font=font, fill=255)

# Display image.
disp.image(image)
disp.display()
'''

GGA = "$GPGGA"
GGA2 = "$GNGGA"
GPN = "$GPNAV"
GPY = "$GPYBM"
STA = "*"

xdi = []
xsi = []
lat = 0
alt = 0

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xdi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches
def find_all_star(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xsi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches

        
        
        
def shift(seq, n=0):
    a = n % len(seq)
    return seq[-a:] + seq[:-a]
        
        
def avg():
    return 0


# Python program to get average of a list 
def Average(lst): 
    
    return sum(lst) / len(lst) 
  
# Driver Code 
lst1 = [0,0] 
lst2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] 
averagex = Average(lst1) 
averagey = Average(lst2)
                
latsig = 0
lngsig = 0
svs = 0
used_svs = 0
bufa = ""


# Printing average of the list 
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)


#os.system("sudo killall -9 bts.py")



tilt = mytilt()
ser = myserial("/dev/serlink1")
bt = mybt();


sdcount = 0


time.sleep(2)

while True:
    bt.accept()
    print 'Accepted connection from ',bt.address

    if ser.isOpen():
        try:
            #write data
            '''
            ser.write("<command>\r\n$$SI\x18\x02\r\n</command>\r\n")
            #ser.write("<command>\r\n$$SI\x18\x02\r\n</command>\r\n".encode())
            time.sleep(0.5)
            cmdSend = "\r\n<command>\r\n"
            cmdSend += "LOG GPGSV ONTIME 1\r\n"
            cmdSend += "LOG SATMSGB ONTIME 1\r\nLOG BESTPOSB ONTIME 1\r\nLOG BESTVELB ONTIME 1\r\nLOG TIMEB ONTIME 1\r\nLOG GPNAV ONTIME 0.1\r\nLOG SATMSG2B ONTIME 1\r\nLOG GPYBM ONTIME 0.1\r\n</command>\r\n"
            #cmdSend += ""
            ser.write(cmdSend);
            ser.send(["LOG GPGSV ONTIME 1", "LOG SATMSGB ONTIME 1", "LOG Silix"])
            
            '''
            #ser.send(["LOG SATMSGB ONTIME 0.5"])
            ser.send(["FIX NONE", "REFAUTOSETUP OFF", "UNLOGALL", "SET PVTFREQ 10", "SET RTKFREQ 10", "LOG COM1 BESTPOSB ONTIME 0.5 0 NOHOLD", "LOG COM1 SATMSGB ONTIME 1 0 NOHOLD", "SAVECONFIG"])

            #ser.send(["FIX NONE", "REFAUTOSETUP OFF", "UNLOGALL", "SET PVTFREQ 2", "SET RTKFREQ 2", "LOG COM1 BESTPOSB ONTIME 0.5 0 NOHOLD", "SAVECONFIG"])
            time.sleep(1.5)
            while True:
                accl_xout_scaled , accl_yout_scaled , accl_zout_scaled = tilt.readAll();
                lst1.pop(0)
                lst1.insert(len(lst1), tilt.get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled))
                

                lst2.pop(0)
                lst2.insert(len(lst2), tilt.get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled))
                
                #print "X Rotation: " , round(Average(lst1) , 4)  ,  " Y Rotation: " , round(Average(lst2) , 4)

                #print "preSeiral"
                #continue
                #response = ser.readline()
                #print response
                #continue
                #try:
                response = ser.readbinary()

                b = bytearray()
                b.extend(response)
                
                msg = b[4] + b[5]*256
                msg_data_len = b[8] + 256*b[9]
                packet_len = len(b)
                print "msg %s msglen %s packet_len %s" % (msg, msg_data_len, packet_len)
                #print b[4]
                if (msg != 42 and msg != 911):
                    continue
                #if msg != 42:
                #    for c in b:
                #        print(c, hex(c), chr(c))

                if msg == 911:
                    bufa = ""
                    i = 0
                    while i < msg_data_len - 4:
                        if (b[i] > 0 and b[i+1]>0 and b[i+2]==0 and b[i+3]>0) or (b[i] > 0 and b[i+1]>0 and b[i+2]==1 and b[i+3]>0):
                            azim = b[i+1] + b[i+2]*256
                            elev = b[i+3]
                            bufa = bufa + "," + str(azim) + "," + str(elev)
                        i += 1                    
                    #print "azim, elev: %s" % bufa
                if (msg != 42):
                    continue


                solution = b[60]
                pos_type = b[64]

                
                '''
                cc = bytearray()
                cc = b[100:104]
                stt = ''.join('{:02x}'.format(x) for x in cc)
                print stt
                #n = unpack('<f', binascii.unhexlify(stt))
                #latsig = n[0]

                cc = bytearray()
                cc = b[104:108]
                stt = ''.join('{:02x}'.format(x) for x in cc)
                print binascii.unhexlify(stt)
                #n = unpack('<d', binascii.unhexlify(stt))
                #lngsig = n[0]
                svs = b[120]
                used_svs = b[121]
                '''
                
                #n=unpack('<d',  stt.decode("hex") )
                #print '%.18f' % n[0]
                #for c in cc:
                #    print(c, hex(c), chr(c))


                b = bytearray()
                b.extend(response)

                cc = bytearray()
                cc = b[68:76]
                stt = ''.join('{:02x}'.format(x) for x in cc)
                #print "stt : %s" % stt
                n = unpack('<d', binascii.unhexlify(stt))
                lat = n[0]

                cc = bytearray()
                cc = b[76:84]
                stt = ''.join('{:02x}'.format(x) for x in cc)
                #print "stt : %s" % stt
                n = unpack('<d', binascii.unhexlify(stt))
                long = n[0]

                cc = bytearray()
                cc = b[84:92]
                stt = ''.join('{:02x}'.format(x) for x in cc)
                #print "stt : %s" % stt
                n = unpack('<d', binascii.unhexlify(stt))
                alt = n[0]


                    #print lat, long , alt

                    #print "======================================================"
                #except Exception, e1:
                #    pass
                
                #continue
                #print response
                #print "info: serial rx [%s]" % response
                di = response.find(GGA)
                if (di == -1):
                    di = response.find(GGA2)
                
                si = response.find(STA, di)
                #print "postSeiral"
                
                print bufa
                strs = str(lat) + "," + str(long) + "," + str(alt) + "," + str(solution) + "," + str(pos_type) + "," + str(latsig) + "," + str(lngsig) + "," + str(svs) + "," + str(used_svs) + bufa + "#" + str(round(Average(lst1) , 1)) + "," + str(round(Average(lst2) , 1)) + "@" + "\r\n".encode("utf-8")
                print strs
                bt.send(strs.decode("utf-8"))
                
                if di != -1 and si != -1 and si > di :
                    print response[di:si]
                    strs = response[di:si] + "#" + str(round(Average(lst1) , 1)) + "," + str(round(Average(lst2) , 1)) + "@" + "\r\n".encode("utf-8")
                    #print type(str(strs))
                    bt.send(strs.decode("utf-8"))

                #if di != -1
                #print "preRecv"
                
                data = bt.get() #client_sock.recv(1024)
                if data:
                    print "info: bt received [%s]" % data
                    parts = data.split("*")
                    subparts = parts[1].split(",")
                    cmd = parts[0]
                    if cmd == "IPSET":
                        i = 1
                    elif cmd == "ADDCMD":
                        i = 1
                    elif cmd == "SETCMD":
                        i = 1
                    elif cmd == "REMCMD":
                        i = 1
                    elif cmd == "PWROFF":
                        os.system ("sudo shutdown now -h")
                    elif cmd == "GETBAT":
                        i = 1
                    elif cmd == "KILLRTK":
                        os.system("sudo killall -9 nt.sh")
                    elif cmd == "RUNRTK":
                        os.system("./nt.sh")
                    else:
                        i = 1
                    
                
                #print "post_rec"
                #print("\r\n" + response)

                #ser.close()
        except Exception, e1:
            print "error communicating...: " + str(e1)
 
    else:
        print "cannot open serial port "
 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
 
client_sock.close()
server_sock.close()
