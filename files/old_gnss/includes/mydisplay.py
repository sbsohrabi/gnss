
import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

class SilixDisplay:
    def __init__(self):
        #initialization
        RST = 24
        DC = 23
        SPI_PORT = 0
        SPI_DEVICE = 0

        disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
        disp.begin() 

        # Clear display.
        disp.clear()
        disp.display()


        # Create blank image for drawing.
        # Make sure to create image with mode '1' for 1-bit color.
        width = disp.width
        height = disp.height
        image = Image.new('1', (width, height))

        # Get drawing object to draw on image.
        draw = ImageDraw.Draw(image)

        # Draw a black filled box to clear the image.
        draw.rectangle((0,0,width,height), outline=0, fill=0)

        # Draw some shapes.
        # First define some constants to allow easy resizing of shapes.
        padding = 2
        shape_width = 5
        top = padding
        bottom = top + shape_width #height-padding
        # Move left to right keeping track of the current x position for drawing shapes.
        x = padding
        # Draw an ellipse.
        #draw.ellipse((x, top , x+shape_width, bottom), outline=255, fill=0)
        x += shape_width+padding
        # Draw a rectangle.
        #draw.rectangle((x, top, x+shape_width, bottom), outline=255, fill=0)
        x += shape_width+padding
        # Draw a triangle.
        #draw.polygon([(x, bottom), (x+shape_width/2, top), (x+shape_width, bottom)], outline=255, fill=0)
        #draw.polygon([(3, 1), (15, 1), (15, 7), (2, 7), (3, 6), (3, 6), (3, 3),(3, 3)], outline=255, fill=0)
        #draw.polygon([(8, 3), (11, 3), (11, 5), (8, 5)], outline=255, fill=1)
        x += shape_width+padding
        # Draw an X.
        #draw.line((x, bottom, x+shape_width, top), fill=255)
        #draw.line((x, top, x+shape_width, bottom), fill=255)
        x += shape_width+padding

        # Load default font.
        font = ImageFont.load_default()

        # Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
        # Some other nice fonts to try: http://www.dafont.com/bitmap.php
        #font = ImageFont.truetype('Minecraftia.ttf', 8)

        # Write two lines of text.
        draw.text((x+10, top-3),    'GNSS',  font=font, fill=255)
        draw.text((x-25, top+4), 'BAT: 75%', font=font, fill=255)
        draw.text((x+45, top+4),    '4G: ON',  font=font, fill=255)
        draw.text((x-10, top+12), 'Mod: NO FIX', font=font, fill=255)
        draw.text((x-10, top+20), 'Sat: 18', font=font, fill=255)

        # Display image.
        disp.image(image)
        disp.display()