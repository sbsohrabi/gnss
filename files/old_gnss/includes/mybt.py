import bluetooth


 
class SilixBT:
    def __init__(self):
        self.server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        #server_sock.settimeout(1)
        self.port = 1
        self.server_sock.bind(('',self.port))
        self.server_sock.listen(1)

    def accept(self):
        self.client_sock,self.address = self.server_sock.accept()
        self.client_sock.settimeout(2)
        self.client_sock.setblocking(False);

    def send(self, strCommand):
        self.client_sock.send(strCommand)
        
    def get(self):
        data = ""
        try:
            data = self.client_sock.recv(1024)
            if (data != ""):
                return data
        except Exception, e1:
            pass
    def close(self):
        self.client_sock.close()
        self.server_sock.close()
