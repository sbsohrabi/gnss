
import array
from struct import unpack
import binascii
import myconstants as constant
import sys

class SilixCalc:
    def __init__(self):
        #initialization
        i = 1


    def find_all(self, a_str, sub):
        xdi = []
        start = 0
        while True:
            start = a_str.find(sub, start)
            if start == -1: return
            xdi.append(start)
            start += len(sub) # use start += 1 to find overlapping matches
    def find_all_star(self, a_str, sub):
        xsi = []
        start = 0
        while True:
            start = a_str.find(sub, start)
            if start == -1: return
            xsi.append(start)
            start += len(sub) # use start += 1 to find overlapping matches
    def shift(self, seq, n=0):
        a = n % len(seq)
        return seq[-a:] + seq[:-a]
            
            
    def avg(self):
        return 0


    # Python program to get average of a list 
    def Average(self, lst): 
        
        return sum(lst) / len(lst) 
      
    
    def get_solution(self, solution): 
        switcher = { 
                    0: "SOL_COMP", 
                    1: "INSUF_OB", 
                    6: "COLD_STR", 
                    } 

        return switcher.get(solution, "nothing") 

    def get_position_type(self, pos_type):
        switcher = { 
                    0: "NONE", 
                    1: "FIXED_POS", 
                    8: "DOP_VEL", 
                    16: "SINGLE", 
                    17: "PSR_DIFF", 
                    18: "SBAS", 
                    34: "NARROW_FL", 
                    49: "WIDE_INT", 
                    50: "NARROW_IN", 
                    51: "SUP_WIDE", 
                    69: "PPP", 
                    } 
        return switcher.get(pos_type, "nothing") 
    
    def compute(self, b, response):
        lat = 0
        alt = 0
        # Driver Code 
        lst1 = [0,0] 
        lst2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] 
        averagex = self.Average(lst1) 
        averagey = self.Average(lst2)

        #i = buf.find(b"\n")
        latsig = 0
        lngsig = 0
        svs = 0
        used_svs = 0
        bufa = ""

        msg = b[4] + b[5]*256
        msg_data_len = b[8] + 256*b[9]
        packet_len = len(b)
        #print "msg %s msglen %s packet_len %s" % (msg, msg_data_len, packet_len)

        if (msg != 42 and msg != 911):
            return ""
        #if msg != 42:
        #    for c in b:
        #        print(c, hex(c), chr(c))

        if msg == 911:
            bufa = ""
            i = 0
            while i < msg_data_len - 4:
                if (b[i] > 0 and b[i+1]>0 and b[i+2]==0 and b[i+3]>0) or (b[i] > 0 and b[i+1]>0 and b[i+2]==1 and b[i+3]>0):
                    azim = b[i+1] + b[i+2]*256
                    elev = b[i+3]
                    bufa = bufa + "," + str(azim) + "," + str(elev)
                i += 1                    
            #print "azim, elev: %s" % bufa

        if msg != 42:
            return ""

        H = 28
        solution = b[H+0] #60]
        pos_type = b[H+4] #64]

        
        '''
        cc = bytearray()
        cc = b[100:104]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        print stt
        #n = unpack('<f', binascii.unhexlify(stt))
        #latsig = n[0]

        cc = bytearray()
        cc = b[104:108]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        print binascii.unhexlify(stt)
        #n = unpack('<d', binascii.unhexlify(stt))
        #lngsig = n[0]
        svs = b[120]
        used_svs = b[121]
        '''
        
        #n=unpack('<d',  stt.decode("hex") )
        #print '%.18f' % n[0]
        #for c in cc:
        #    print(c, hex(c), chr(c))


        b = bytearray()
        b.extend(response)

        cc = bytearray()
        cc = b[H+8:H+16] #68:76]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "lat_hex : %s" % stt
        n = unpack('<d', binascii.unhexlify(stt))
        lat = n[0]

        cc = bytearray()
        cc = b[H+16:H+24] #76:84]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "stt : %s" % stt
        n = unpack('<d', binascii.unhexlify(stt))
        long = n[0]

        cc = bytearray()
        cc = b[H+24:H+32] #84:92]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "stt : %s" % stt
        n = unpack('<d', binascii.unhexlify(stt))
        alt = n[0]

        cc = bytearray()
        cc = b[H+56:H+60] #84:92]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "stt : %s" % stt
        n = unpack('<f', binascii.unhexlify(stt))
        diff_age = n[0]

        cc = bytearray()
        cc = b[H+40:H+44] #84:92]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "stt : %s" % stt
        n = unpack('<f', binascii.unhexlify(stt))
        lat_sig = n[0]

        cc = bytearray()
        cc = b[H+44:H+48] #84:92]
        stt = ''.join('{:02x}'.format(x) for x in cc)
        #print "stt : %s" % stt
        n = unpack('<f', binascii.unhexlify(stt))
        lon_sig = n[0]

        #print lat, long , alt
        #print "======================================================"
        #except Exception, e1:
        #    pass
        #return ""
        #print response
        #print "info: serial rx [%s]" % response
        di = response.find(constant.GGA)
        if (di == -1):
            di = response.find(constant.GGA2)
        
        si = response.find(constant.STA, di)
        #print "postSeiral"
        sat_tracked = b[H+64]
        sv_used = b[H+65]
        if sat_tracked ==0 or sat_tracked > 35 or sat_tracked == 0 or sat_tracked > 35 or lat_sig < 0.000000000001 or lon_sig < 0.000000000001: 
            return ""
        #print bufa
        # 2,lat,lng,alt,lat_t,lng_typ,tx,ty,tmp,stat,b1,b2
        strs = "2," + \
                str(lat) + "," + str(long) + "," + str(alt) + ",e,n,0,0,37.2,25,52," + str(solution) + "," + str(pos_type) + "," + str(sat_tracked) + "," + str(sv_used) + "," + str(diff_age) + "".encode("utf-8")
        '''
        + \
        str(solution) + "," + str(pos_type) + "," + \
        str(latsig) + "," + str(lngsig) + "," + \
        str(svs) + "," + str(used_svs) + \
        bufa + \
        "#" + str(round(self.Average(lst1) , 1)) + "," + str(round(self.Average(lst2) , 1)) + "@" + "\r\n"
        '''
        #strs = "2," + \
        #        str(lat) + "," + str(long) + "," + str(alt) + "," + \
        #        str(solution) + "," + str(pos_type) + "," + \
        #        str(latsig) + "," + str(lngsig) + "," + \
        #        str(svs) + "," + str(used_svs) + \
        #        bufa + \
        #        "#" + str(round(self.Average(lst1) , 1)) + "," + str(round(self.Average(lst2) , 1)) + "@" + "\r\n".encode("utf-8")
        
        #sys.stdout.write('\033[2K\033[1G')
        #sys.stdout.write(strs)
        #sys.stdout.flush()
        
        
        #print strs
        print self.get_solution(solution) + "," + self.get_position_type(pos_type) + "," + str(lat_sig) + "," + str(lon_sig)
        if di != -1 and si != -1 and si > di :
            print response[di:si]
            strs = response[di:si] + "#" + str(round(Average(lst1) , 1)) + "," + str(round(Average(lst2) , 1)) + "@" + "\r\n".encode("utf-8")
            print type(str(strs))
            #bt.send(strs.decode("utf-8"))
        #if di != -1
            #print "preRecv"
        return strs
