import smbus
import math
from mycalc import SilixCalc


 
class SilixTilt:
    def __init__(self):
        try:
            # Register
            self.power_mgmt_1 = 0x6b
            self.power_mgmt_2 = 0x6c
            self.bus = smbus.SMBus(1)                            # bus = smbus.SMBus(0) fuer Revision 1
            self.i2c_address = 0x68                                  # via i2cdetect
            self.bus.write_byte_data(self.i2c_address, self.power_mgmt_1, 0)   # Activate to be able to address the module
            self.tiltPresent = True
        except Exception, e1:
            #print "error ...: " + str(e1)
            print "warning: tilt is not available"
            self.tiltPresent = False
            
    def read_byte(self, reg):
        return self.bus.read_byte_data(self.i2c_address, reg)
     
    def read_word(self, reg):
        h = self.bus.read_byte_data(self.i2c_address, reg)
        l = self.bus.read_byte_data(self.i2c_address, reg+1)
        value = (h << 8) + l
        return value
    
    def read_word_2c(self, reg):
        val = self.read_word(reg)
        if (val >= 0x8000):
            return -((65535 - val) + 1)
        else:
            return val
     
    def dist(self, a,b):
        return math.sqrt((a*a)+(b*b))
     
    def get_y_rotation(self, x,y,z):
        radians = math.atan2(x, self.dist(y,z))
        return -math.degrees(radians)
     
    def get_x_rotation(self, x,y,z):
        radians = math.atan2(y, self.dist(x,z))
        return math.degrees(radians)

    def readAll(self):
        accl_xout = self.read_word_2c(0x3b)
        accl_yout = self.read_word_2c(0x3d)
        accl_zout = self.read_word_2c(0x3f)
        return [accl_xout / 16384.0, accl_yout / 16384.0,  accl_zout / 16384.0]

    def wrapedResult(self):
        if (self.tiltPresent == True):
            lst1 = [0,0] 
            lst2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] 
            accl_xout_scaled , accl_yout_scaled , accl_zout_scaled = self.readAll();
            lst1.pop(0)
            lst1.insert(len(lst1), self.get_x_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled))
            lst2.pop(0)
            lst2.insert(len(lst2), self.get_y_rotation(accl_xout_scaled, accl_yout_scaled, accl_zout_scaled))
            
            print "X Rotation: " , round(SilixCalc.Average(lst1) , 4)  ,  " Y Rotation: " , round(SilixCalc.Average(lst2) , 4)
