#!/usr/bin/python
'''
import time

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

'''

import time
import bluetooth

import math
import os
from myserial import myserial
from mytilt import mytilt
from mybt import mybt
         
import array
from struct import unpack
import binascii

import subprocess


'''
RST = 24
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
disp.begin()

# Clear display.
disp.clear()
disp.display()


# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = 2
shape_width = 5
top = padding
bottom = top + shape_width #height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = padding
# Draw an ellipse.
#draw.ellipse((x, top , x+shape_width, bottom), outline=255, fill=0)
x += shape_width+padding
# Draw a rectangle.
#draw.rectangle((x, top, x+shape_width, bottom), outline=255, fill=0)
x += shape_width+padding
# Draw a triangle.
#draw.polygon([(x, bottom), (x+shape_width/2, top), (x+shape_width, bottom)], outline=255, fill=0)
#draw.polygon([(3, 1), (15, 1), (15, 7), (2, 7), (3, 6), (3, 6), (3, 3),(3, 3)], outline=255, fill=0)
#draw.polygon([(8, 3), (11, 3), (11, 5), (8, 5)], outline=255, fill=1)
x += shape_width+padding
# Draw an X.
#draw.line((x, bottom, x+shape_width, top), fill=255)
#draw.line((x, top, x+shape_width, bottom), fill=255)
x += shape_width+padding

# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
#font = ImageFont.truetype('Minecraftia.ttf', 8)

# Write two lines of text.
draw.text((x+10, top-3),    'GNSS',  font=font, fill=255)
draw.text((x-25, top+4), 'BAT: 75%', font=font, fill=255)
draw.text((x+45, top+4),    '4G: ON',  font=font, fill=255)
draw.text((x-10, top+12), 'Mod: NO FIX', font=font, fill=255)
draw.text((x-10, top+20), 'Sat: 18', font=font, fill=255)

# Display image.
disp.image(image)
disp.display()
'''

GGA = "$GPGGA"
GGA2 = "$GNGGA"
GPN = "$GPNAV"
GPY = "$GPYBM"
STA = "*"

xdi = []
xsi = []
lat = 0
alt = 0

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xdi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches
def find_all_star(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        xsi.append(start)
        start += len(sub) # use start += 1 to find overlapping matches

        
        
        
def shift(seq, n=0):
    a = n % len(seq)
    return seq[-a:] + seq[:-a]
        
        
def avg():
    return 0


# Python program to get average of a list 
def Average(lst): 
    
    return sum(lst) / len(lst) 
  
# Driver Code 
lst1 = [0,0] 
lst2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] 
averagex = Average(lst1) 
averagey = Average(lst2)
                
latsig = 0
lngsig = 0
svs = 0
used_svs = 0
bufa = ""


# Printing average of the list 
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)


#os.system("sudo killall -9 bts.py")


# ser1 = radio
# ser2 = k7 com2
# ser3 = k7 com1
# ser4 = mcu ser

#tilt = mytilt()
#k7_com2 = myserial("/dev/serlink2")
#radio_com = myserial("/dev/serlink1")
#mcu_com = myserial("/dev/serlink4")
k7_com1 = myserial("/dev/serlink3")
bt = mybt();


sdcount = 0


time.sleep(2)

while True:
    bt.accept()
    print 'Accepted connection from ',bt.address

    if k7_com1.isOpen():
        try:
            k7_com1.send(["FIX NONE", "REFAUTOSETUP OFF", "UNLOGALL", "SET PVTFREQ 10", "SET RTKFREQ 10", "LOG COM1 BESTPOSB ONTIME 0.5 0 NOHOLD", "LOG COM1 SATMSGB ONTIME 1 0 NOHOLD", "SAVECONFIG"])
            time.sleep(1.5)
            while True:
                response = k7_com1.read()
                print response
                '''
                b = bytearray()
                b.extend(response)
                
                msg = b[4] + b[5]*256
                msg_data_len = b[8] + 256*b[9]
                packet_len = len(b)
                print "msg %s msglen %s packet_len %s" % (msg, msg_data_len, packet_len)
                '''
                #strs = "@" + "\r\n".encode("utf-8")
                #print strs
                #bt.send(strs.decode("utf-8"))
                
                data = bt.get() #client_sock.recv(1024)
                if data:
                    print "info: bt rx :: [%s]" % data
                    if data.find(',') != -1:
                        parts = data.split("#")
                        subparts = parts[1].split(",")
                        cmd = parts[0]
                    
                
                #print "post_rec"
                #print("\r\n" + response)

                #k7_com1.close()
        except Exception, e1:
            print "error communicating...: " + str(e1)
 
    else:
        print "cannot open serial port "
 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
 
client_sock.close()
server_sock.close()
