#!/usr/bin/python

import sys
sys.path.append('/home/pi/silix-gnss/includes/')

import time
import bluetooth
import math
import os


import array
from struct import unpack
import binascii

import subprocess

from myserial import SilixSerial
from mytilt import SilixTilt
from mybt import SilixBT
from mydisplay import SilixDisplay
from mycalc import SilixCalc
from myparser import SilixParser
from myutils import SilixUtils

import myconstants as constant
#constant.


print ""
print "initializing..."

#salar edited this 1.11xy

# Printing average of the list 
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)
#print "Average of the list =", round(Average(lst1) , 4), " , ", round(averagey, 4)


#os.system("sudo killall -9 bts.py")



radio_com = SilixSerial("/dev/serlink1")
k7_com2   = SilixSerial("/dev/serlink2")
k7_com1   = SilixSerial("/dev/serlink3")
mcu_com   = SilixSerial("/dev/serlink4")

tilt = SilixTilt()
bt   = SilixBT();
disp = SilixDisplay();
util = SilixUtils();
calc = SilixCalc();
pars = SilixParser();


loopCond = True

time.sleep(2)

while loopCond:

    print "waiting for bt connection"
    bt.accept() 
    print 'Accepted connection from ',bt.address

    if k7_com1.isOpen() == False:
        print "cannot open serial port "
        break

    try:
        #write data

        util.initK7(k7_com1)
        time.sleep(1.5)

        while loopCond:
        
            tilt.wrapedResult();

            #print "preSeiral"
            #continue
            #response = k7_com1.readline()
            #print response
            
            
            response = k7_com1.readbinary()
            if (response != ""):
                b = bytearray()
                b.extend(response)
                strs = calc.compute(b, response)
                if strs == "":
                    continue
                bt.send(strs.decode("utf-8"))
            data = bt.get() #client_sock.recv(1024)
            if data:
                print "info: bt rx :: [%s]" % data
                resp = pars.doAction(mcu_com, data)
                if resp == 10:
                    loopCond = False
                    print "loop is off      .......  "
                elif resp == 11:
                    loopCond = True
                    print "    .......         loop is on      .......  "
                
            
            #print "post_rec"
            #print("\r\n" + response)

            #k7_com1.close()
    except Exception, e1:
        print "error ...: " + str(e1)

 
 #if (data == 'e'):
 #  print 'Exit'
 #  break
bt.close()
