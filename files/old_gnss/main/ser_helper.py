#!/usr/bin/python

import sys
sys.path.append('/home/pi/silix-gnss/includes/')

import time
import math
import os


import array
from struct import unpack
import binascii

import subprocess

from myserial import SilixSerial
from myparser import SilixParser
from myutils import SilixUtils

import myconstants as constant
#constant.


print ""
print "initializing..."

mcu_com   = SilixSerial("/dev/serlink4")

util = SilixUtils();
pars = SilixParser();

loopCond = True
i = 0
while loopCond:

    if mcu_com.isOpen() == False:
        print "cannot open serial port "
        break

    try:
        #write data

        while loopCond:
        
        
            response = mcu_com.readline0()
            si = response.find('\xaa\x44')
            
            di = response.find('\x44\xaa')
            if si >=0 and di > si:
                #print "si is %d and di is %d" % (si,di)
                if di - si == 3:
                    #print "info: rx from mcu : 0x{0:02x}".format(ord(response[di-1:di]))
                    #print "si is %d and di is %d" % (si,di)
                    #print "---"
                    if len(response[di-1:di]) > 0:
                        resp = ord(response[di-1:di])
                        print "info: byte is %d" % resp
                        if resp == 40:
                            #os.system('sudo shutdown -h now')
                            mcu_com.write('s')
                            #print "shutting down ......  "
                            #loopCond = False
                        elif resp == 41:
                            loopCond = True
                            #print "report stat"
                elif di-si > 3:
                    resp = response[si+2:di]
                    print "info: rx from mcu : " + resp
                    if resp == "shut":
                        os.system('sudo shutdown -h now')
                    
            i = i + 1

    except Exception, e1:
        print "error ...: " + str(e1)

 
 #if (data == 'e'):
 #  print 'Exit'
 #  break

