#!/usr/bin/python

import serial

class myserial:
    def __init__(self, port):
        #initialization and open the port

        #possible timeout values:
        #    1. None: wait forever, block call
        #    2. 0: non-blocking mode, return immediately
        #    3. x, x is bigger than 0, float allowed, timeout block call

        ser = serial.Serial()
        ser.port = port
        ser.baudrate = 115200
        ser.bytesize = serial.EIGHTBITS #number of bits per bytes
        ser.parity = serial.PARITY_NONE #set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE #number of stop bits
        #ser.timeout = None          #block read
        ser.timeout = 1            #non-block read
        ser.xonxoff = False     #disable software flow control
        ser.rtscts = False     #disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 2     #timeout for write

        try:
            ser.open()
        except Exception, e:
            print "error open serial port, matbe device is not connected"
            exit()

        self.buf = bytearray()
        self.s = ser

    def readline(self):
        i = self.buf.find(b"\n")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i) 
            i = data.find("\r\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                return r
            else:
                self.buf.extend(data)

    def readbinary(self):
        si = self.buf.find('\xaa\x44\x12')
        if si >= 0:
            msg_len = 0
            if len(self.buf) >= si + 28:
                msg_len = self.buf[si+8] + 256*self.buf[si+9]
                
            #print "msg_len %s " % msg_len
            if msg_len > 0:
                if si + 28 + msg_len <=  len(self.buf):
                    r = self.buf[si:si + 28 + msg_len]
                    self.buf = self.buf[si + 28 + msg_len + 1:]
                    return r
            
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            self.buf = self.buf + data

            si = self.buf.find('\xaa\x44\x12')
            if si >= 0:
                msg_len = 0
                if len(self.buf) >= si + 28:
                    msg_len = self.buf[si+8] + 256*self.buf[si+9]
                    
                #print "si: %s" % si
                #print "msg_len %s " % msg_len
                #print "len %s" % len(self.buf)
                if msg_len > 0:
                    if si + 28 + msg_len <=  len(self.buf):
                        r = self.buf[si:si + 28 + msg_len]
                        self.buf = self.buf[si + 28 + msg_len + 1:]
                        return r
                    else:
                        self.buf.extend(data)
                        #print "[extend1] %s" % len(self.buf)
                else:
                    self.buf.extend(data)
                    #print "[extend2] %s" % len(self.buf)
            else:
                self.buf.extend(data)
                #print "[extend3] %s" % len(self.buf)
            
                
    def read(self):
        i = max(1, min(2048, self.s.in_waiting))
        data = self.s.read(i) 
        return data

    def write(self, strsend):
        self.s.write(strsend.encode())
        return
        
    def isOpen(self):
        return self.s.isOpen()
        
    def send(self, commands=[]):
        cmdSend = "\r\n<command>\r\n"
        for cmd in commands:
            cmdSend += cmd + "\r\n"
        cmdSend += "</command>\r\n"
        print cmdSend
        self.write(cmdSend)
        
        
        